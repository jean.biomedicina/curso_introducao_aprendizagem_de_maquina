# Bem-vindo ao Curso


### Pós-graduação em Ciência de Dados e Machine Learning

#### Módulo 3 - Data Mining e Machine Learning

#### Disciplina: **Introdução a Aprendizagem De Máquina**

#### Turma: **A**     

#### Código da Turma: *lwnlpzm*

#### Professor: Professor MSc. Ricardo José Menezes Maia.  http://lattes.cnpq.br/0706885145380777
* Doutorado, Universidade de Brasília UNB - Ciência da Computação - 2019 - em andamento - Research Privacy-Preserving Automated Machine Learning
* Mestrado, Universidade de São Paulo USP - Escola Politécnica, Engenharia de Computação - 2008 a 2010
* Bacharelado em Ciência da Computação, Universidade Federal do Amazonas - 1999 a 2003
* Técnico em Programação, Fundação de Ensino e Pesquisa Matias Machline - Sharp do Brasil - 1992 a 1994.

Desejo que nossa experiência neste curso seja a melhor possível!

Abaixo está o sumário do curso e para baixar todo conteúdo que está versionado no git clique no link
https://gitlab.com/ricardo.jmm/curso_introducao_aprendizagem_de_maquina/-/archive/master/curso_introducao_aprendizagem_de_maquina-master.zip

Após isso descompacte o arquivo em uma pasta da sua escolha

Abra o jupyter notebook. Lembrando que há inúmeras formas de fazer. Exemplo no linux e macos teria uma das formas #jupyter notebook

Após abrir o jupyter você irá clicar em File->Open e escolher o notebook "BemVindoAoCurso.ipynb" que estará na raiz da pasta. 

Este jupyter terá o sumário para você navegar entre os assuntos.

Neste curso os exercícios que estarão na pasta exercícios sempre solicitados que sejam entregues.

Plano de Ensino está no arquivo PlanoDeEnsino-CursoDeCienciaDeDadosEMachineLearning-IntroducaoAAprendizagemDeMaquina.docx
[Plano de Ensino](PlanoDeEnsino-CursoDeCienciaDeDadosEMachineLearning-IntroducaoAAprendizagemDeMaquina.docx)

Será feita 1 avaliação que serão disponibilizada no https://classroom.google.com. A entrega será para dia 17/07/2020.

A proposta de ensino é:

* 24/6/2020 - (19h a 23h) 01_Introducao_Machine_Learning / 02_Regressoes_Lineares
* * Link para aula https://meet.google.com/gon-vwnx-tgo

* 25/6/2020 - (19h a 23h) 03_Regressao_Logistica / 04_K-Nearest-Neighbors
* * Link para aula https://meet.google.com/yfi-jcav-rmm

* 27/6/2020 - (8h a 12h) 07_K-Means-Clustering / 08_Analise_do_componente_principal_PCA 
* * Link para aula https://meet.google.com/ytm-yjdd-ohq

* 30/6/2020 - (19h a 23h) 06_Sistemas_de_recomendacao
* * Link para aula https://meet.google.com/iob-oyhq-fpd

* 02/7/2020 - (19h a 23h) 05_Arvores_de_decisao_e_Florestas_Aleatorias
* * Link para aula https://meet.google.com/ijd-xmwm-egg

* 04/7/2020 - (8h a 12h) 09_Support_Vector_Machines
* * Link para aula https://meet.google.com/oux-bihv-kfb

* 07/7/2020 - (19h a 23h) 10_RedesNeurais
* * Link para aula https://meet.google.com/fxs-uqwa-sgs

* 09/7/2020 - (19h a 23h) 11_NaiveBayes
* * Link para aula https://meet.google.com/suq-fpqx-toz

* 11/7/2020 - (8h a 12h) 12_Automl / 13_projetos 
* * Link para aula https://meet.google.com/kpd-uidx-bgg

### Referências

##### Básica

James, G., Witten, D., Hastie, T., Tibshirani, R. (2013). <I>An Introduction to Statistical Learning with Applications in  R</I>,  Springer Science+Business Media, New York.
http://www-bcf.usc.edu/~gareth/ISL/index.html   [Download Livro](http://faculty.marshall.usc.edu/gareth-james/ISL/ISLR%20Seventh%20Printing.pdf)


Hastie, T., Tibshirani, R., Friedman, J. (2009). <I>Elements of Statistical Learning</I>, Second Edition, Springer Science+Business Media, New York.
http://statweb.stanford.edu/~tibs/ElemStatLearn/

Ethem Alpaydin. Introduction to Machine Learning: Adaptive Computation and Machine Learning series. MIT Press, 2014, ISBN 0262028182, 9780262028189.

Shai Shalev-Shwartz, Shai Ben-David. Understanding Machine Learning: From Theory to Algorithms. Cambridge University Press, 2014. ISBN	1107057132, 9781107057135
	
##### Complementar

Christopher M. Bishop. Pattern Recognition and Machine Learning: Information Science and Statistics, Springer, 2006. ISSN 1613-9011. ISBN	0387310738, 9780387310732.

#### Conteúdo e referências
* Livro Referência
* Gareth James, An Introduction to Statistical Learning

* Introdução
* * Capítulo 1 e 2
* * * Gareth James, An Introduction to Statistical Learning

* Regressão Linear
* * Capítulo 2 e 3
* * * Gareth James, An Introduction to Statistical Learning

* Regressão Logística
* * Seção 4 a 4.3
* * * Gareth James, An Introduction to Statistical Learning

* KNN
* * Seção 4.6
* * * Gareth James, An Introduction to Statistical Learning

* Árvore de decisão e Florestas aleatórias
* * Seção 8
* * * Gareth James, An Introduction to Statistical Learning

* SVM
* * Seção 9
* * Gareth James, An Introduction to Statistical Learning

* KMeans Clustering
* * Seção 10
* * * Gareth James, An Introduction to Statistical Learning

* PCA
* * Seção 10.2
* * * Gareth James, An Introduction to Statistical Learning

* Sistemas de recomendação
* * https://www.ibm.com/developerworks/br/local/data/sistemas_recomendacao/

## Sumário:

* [01_Introducao_Machine_Learning](01_Introducao_Machine_Learning)
* [02_Regressao_Linear](02_Regressao_Linear)
* [03_Regressao_Logistica](03_Regressao_Logistica)
* [04_K-Nearest-Neighbors](04_K-Nearest-Neighbors)
* [05_K-Means-Clustering](05_K-Means-Clustering)
* [06_Analise_do_componente_principal_PCA](06_Analise_do_componente_principal_PCA)
* [07_Arvores_de_decisao_e_Florestas_Aleatorias](07_Arvores_de_decisao_e_Florestas_Aleatorias)
* [08_Sistemas_de_recomendacao](08_Sistemas_de_recomendacao)
* [09_Support_Vector_Machines](09_Support_Vector_Machines)
* [10_RedesNeuraisArtificiais](10_RedesNeuraisArtificiais)
* [11_NaiveBayes](11_NaiveBayes)
* [12_Automl_AutomatedMachineLearning](12_Automl_AutomatedMachineLearning)
* [13_Projetos](13_Projetos)